<?php 
session_start();
require "ServerClass.php";
$serv = new Server();

?>

<html>
<head>
    <meta charset="UTF-8">
    
    <?php $serv->style() ?>
    <?php
        $title = "Test";
        echo "<title>$title</title>";
    ?>
</head>
<body>
<?php 
$serv->menu();
if($_SERVER["REQUEST_METHOD"] == "GET"){
    echo $_GET["msg"]??"";
}
?>
<?php
class Something{
    public function createTable($x,$y){
        echo "<table>";
        $sumsX = array();
        $sumOfAllSums=0;
        $allNumbersArr=array();
        for($i=0;$i<$y;$i++){
            $sumsX[$i]=0;
        }
        for($i=0;$i<$x;$i++){
            echo "<tr>";
            $sumY=0;
            for($j=0;$j<$y+1;$j++){
                if($j==$y){
                    echo "<td style='color:red;'><center>$sumY</center></td>";    //right column
                }
                else{
                    do{
                        $rnd = rand(1,$x*$y);
                    } while($this->search($rnd,$allNumbersArr)==1);
                    
                    $allNumbersArr[]=$rnd;
                    $sumsX[$j]+=$rnd;
                    $sumY+=$rnd;
                    $sumOfAllSums+=$rnd*2;
                    echo "<td><center>$rnd</center></td>";
                }
            }
            echo "</tr>";
        }
        echo "<tr>";
        for($i=0;$i<$y;$i++){                                                     //bottom row
            echo "<td style='color:green;'><center>$sumsX[$i]</center></td>";
        }
        echo "<td style='color:yellow;'><center>$sumOfAllSums</center></td>"."</tr>"."</table>";       //sum of sums
    }
    public function search($value,$arr){
        foreach($arr as $x){
            if($x == $value)return 1;
        }
        return 0;
    }
    public function numbersList($x){
        for($i=1;$i<$x;$i++){
            if($i%8==0)echo "<b>$i</b> ";
            else echo "$i ";
        }
    }
    public function replaceTest(){
        $str="This string will be replaced, I say again, this string will be replaced.";
        $what=array("This","string","will be"," ");
        $to=array("The following","array of characters","has been","_");
        echo '<br>'.$str.'<br>';
        echo str_ireplace($what,$to,$str)."<br>";                                   //str_ireplace()

        $whatn="I_say";
        $ton="I_proclaim";
        echo str_replace($whatn,$ton,str_ireplace($what,$to,$str)).'<br><br>';      //str_replace()
    }
    public function palindrome($str){                                               //strrev()
        return $str==strrev($str);
    }
}
//$t = new Something();
//$t->createTable(10,10);
//$t->numbersList(1000000);
//$t->replaceTest();
function stringFuncs($t){
    $str = "palindromassamordnilap";
    if($t->palindrome($str))echo "raidžių seka \"$str\" yra palindromas".'<br><br>';
    else echo "raidžių seka \"$str\" nėra palindromas"."<br><br>";

    $str = "<br><button>button</button>";
    echo strip_tags($str)."<br><br>";                                                   //strip_tags()

    $str = "This is a string.";
    echo $str.'<br>';
    echo "The letter i is repeated ".substr_count($str,"i")." times in the last sentence."; //substr_count()
    echo '<br>'."Position of 'string' is ".strpos($str,"string").'.<br><br>';               //strpos()

    $str ="This is a sentence.";
    echo wordwrap($str,4,'<br>',true).'<br><br>';                                       //wordwrap()

    $str = "This sentence is an array.".'<br>';
    $arr = explode(' ',$str);                                                           //explode()
    echo "1. ";
    for($i=0;$i<sizeof($arr);$i++){
        echo $arr[$i].' ';
    }

    $str = implode(' ',$arr);                                                           //implode()
    $str1=$str;
    $str = str_replace(" is "," is no longer ",$str);
    echo "2. ".$str;

    echo "the md5 hash of sentence 2 is ".md5($str).'<br><br>';

    similar_text($str,$str1,$sim);
    echo "Silmilarity of sentence 1 and 2 = ".$sim.' %';

    echo "<br>Arrays<br>";

    $keys = array("key1","key2");
    $values = array("value1","value2");
    $assocArr = array();
    for($i=0;$i<sizeof($keys);$i++)$assocArr[$keys[$i]]=$values[$i];
    for($i=0;$i<sizeof($keys);$i++)echo $assocArr[$keys[$i]]."<br>";    
}
?>
</body>
</html>