<?php
class Server{
        public function trim_full($str,$no_space=true){//trims all spaces from a string (or leaves one in between)
            $ret="";
            foreach(explode(' ',$str) as $x)if($x!=''){
                if(!$no_space)$ret = $ret.$x.' ';
                else $ret = $ret.$x;
            }
            return trim($ret);
        }
        public static function style(){
            echo '
            <style>
                :root{
                    --font-size:2.5vmin;
                }
                a{
                    color:firebrick;
                    font-size: calc(var(--font-size) * 1.2);
                    text-decoration-line: none;
                    padding-right: 10px;
                }
                a:hover{
                    color:fuchsia;
                    font-weight: bold;
                }
                body{
                    background: lightgray;
                    color:black;
                    font-size:var(--font-size);
                }
                b{
                    color:red;
                }
            </style>';
        }
        public static function menu(){
            echo '
            <center class="menu">
                <a href="index.php">Index</a>
                <a href="login.php">Login</a>
                <a href="form.php">Register</a>
                <a href="objects.php">Objects</a>
                <a href="studentTable.php">Student table</a>';
                if($_SESSION["login"]??false==true){
                    echo '<a href="profile.php">Profile</a>
                    <a href="logout.php">Log out</a>';
                }
                if($_SESSION["admin"]??false==true){
                    echo '<a href="adminPage.php">Admin</a>';
                }
                

             echo '</center>';
        }
        public function test_input($data,$sql=0){
            $data=trim($data);
            $data=stripslashes($data);
            $data=htmlspecialchars($data);
            if($sql==1){
                $conn = new mysqli("localhost","root","");
                $data=mysqli_real_escape_string($conn,$data);
                mysqli_close($conn);
            }
            return $data;
        }
        public function tryRegister($username,$password,$name,$city,$gender,$hobby,$about,$blood){
            $db = "test";
            $password = password_hash($password,PASSWORD_DEFAULT);
            $conn = new mysqli("localhost","root","",$db);
            if(($username != $this->test_input($username,1)) || ($password != $this->test_input($password,1)) ||
                $username=="" || $password==""){
                return 3;
            }
            else if(strlen($username)>20)return 4;
            else if(strlen($name)>40)return 6;
            else if(strlen($hobby)>20)return 7;
            else if(strlen($gender)>20 || strlen($city)>30 || strlen($blood)>10)return 8;

            if(mysqli_connect_error()){
                echo "<script>console.log(".mysqli_connect_error().")</script>";
            }
            else{
                echo "<script>console.log('connection okay')</script>";
            }
            $sql_insert = "INSERT INTO users (`username`, `password`, `name`, `city`, `gender`, hobby, about, blood) VALUES ('$username','$password','$name','$city','$gender','$hobby','$about','$blood');";
            $sql_search = "SELECT `username`, `password` FROM users WHERE username LIKE '$username'";
            $searchResult = mysqli_query($conn,$sql_search);
            if(mysqli_num_rows($searchResult)>0){
                mysqli_close($conn);
                return 2;
            }
            if(mysqli_query($conn,$sql_insert)){
                echo "<script>console.log('Successfully registered')</script>";
                mysqli_close($conn);
                return 0;
            }
            else{
                echo "<script>console.log(\"error:". mysqli_error($conn) ."\")</script>";
                mysqli_close($conn);
                return 1;
            }
        }
        public function userData($username){
            $db = "test";
            $conn = new mysqli("localhost","root","",$db);
            if(mysqli_connect_error()){
                echo "<script>console.log(".mysqli_connect_error().")</script>";
            }
            $sql_search = "SELECT * FROM users WHERE username LIKE '$username'";
            $searchResult = mysqli_query($conn,$sql_search);
            if($searchResult){
                echo "<script>console.log('Profile found')</script>";
                mysqli_close($conn);
                return mysqli_fetch_all($searchResult);
            }
            else{
                echo "<script>console.log(\"error:". mysqli_error($conn) ."\")</script>";
                mysqli_close($conn);
                return 1;
            }
        }
    };
?>