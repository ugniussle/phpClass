
<?php
    session_start();
    require "ServerClass.php";
    $serv = new Server();
    $serv->style();
    $serv->menu();
    if($_SESSION["login"]==false){
        header("Location: login.php?prev=".htmlspecialchars($_SERVER["PHP_SELF"]));
        exit();
    }
    else{
        echo "Hello, ".$_SESSION["username"];
    }
    class obj{
        public $var="text";
        public function obj(){
            echo "sukurtas obj<br>";
        }
        public function __destruct(){
            echo "sunaikintas obj<br>";
        }
    }
    class obj2 extends obj{
        public function obj2($property){
            echo "<br>sukurtas obj2<br>";
            echo $property."<br>";
        }
        /* public function __destruct(){
            echo "sunaikintas obj2<br>";
        } */
        public static function pi(){
            return 3.14;
        }
    }

    $obj = new obj2("kazkas");
    echo $obj->var."<br>";

    echo obj2::pi()."<br>";




    
?>