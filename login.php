<?php 
    session_start();
    require "ServerClass.php";
    $serv = new Server();
?>
<head>
    <?php $serv->style() ?>
</head>
<?php 
    $serv->menu();
    if($_SESSION["login"]??""==true){
        echo "You are already logged in.";
        exit();
    }
    if($_GET["prev"]??""!=""){
        echo "Please log in to see contents of the page.";
    }
?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?>">
    Username:<input type="text" name="username" value="<?php echo $_POST["username"]??"" ?>"><br>
    Password:<input type="password" name="password"><br>
    <input type="submit" name="submit" value="Login">
</form>
<?php
    if($_SERVER["REQUEST_METHOD"] == "POST"){  //Login
        $db = "test";
        $username = test_input($_POST["username"],1);
        $password = $_POST["password"];
        echo "<script>console.log('".$password."')</script>";
        $conn = new mysqli("localhost","root","",$db);
        if(mysqli_connect_error()){
            echo "<script>console.log('".mysqli_connect_error()."')</script>";
        }
        $sql_search = "SELECT * FROM `users` WHERE `username`='$username';";  //change password verification to password_verify()
        $searchResult = mysqli_query($conn,$sql_search);
        $info = mysqli_fetch_array($searchResult);
        if(password_verify($password,$info["password"])){
            if($info["admin"]==true) $_SESSION["admin"]=true;
            $_SESSION["login"]=true;
            $_SESSION["username"]=$username;
            echo "Successfully logged in.";
            $url="";
            if(($_GET["prev"]??"")!=""){
                $url=$_GET["prev"];
            }
            else{
                $url="index.php";
            }
            header("Location: ".$url);
            exit();
        }
        else{
            echo "\nSuch an account does not exist. Please try again.";
        }
    }
    function test_input($data){
        $data=trim($data);
        $data=stripslashes($data);
        $data=htmlspecialchars($data);
        return $data;
    }
?>