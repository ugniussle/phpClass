<?php
require 'ServerClass.php';
class Form extends Server{
    public function registerModal(){
        echo "<div class=\"modal\" id=\"modal\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-body\">";
        $required=["gender","city","name","username","password"];
        $requiredLt=["lytis","miestas","vardas ir pavardė","prisijungimo vardas","slaptažodis"];
        $text="";
        $missing=$i=0;
        foreach($required as $field){
            if(($_POST[$field] ?? "")==""){
                $text=$text.$requiredLt[$i].'&nbsp&nbsp&nbsp';
                $missing++;
            }
            $i++;
        }
        if(($_POST["ifHobby"]??"")=="on" && ($_POST["hobby"]??"")==""){
            $text=$text."hobis".'&nbsp&nbsp&nbsp';
            $missing++;
        }
        if($missing>0) echo "Neužpildėte visų privalomų laukų. Prašome užpildyti: <br><b>$text</b>";
        else if($missing==0){
            $username = $this->test_input($_POST["username"],1);
            $password = $_POST["password"];
            $name = $this->test_input($_POST["name"],1);
            $city = $this->test_input($_POST["city"],1);
            $gender = $this->test_input($_POST["gender"],1);
            $hobby = $this->test_input($_POST["hobby"]??"",1);
            $about = $this->test_input($_POST["about"]??"",1);
            $blood = $this->test_input($_POST["color"]??"#000000",1);
            $result = $this->tryRegister($username,$password,$name,$city,$gender,$hobby,$about,$blood);
            switch($result){
                case 0:
                    echo "Sėkmingai užsiregistravote,";
                    $name = explode(' ',$name);
                    foreach($name as $x)echo ' '.trim($x);
                    echo '.';
                    //header("Location: index.php");
                    break;
                case 1: echo "Klaida."; break;
                case 2: echo "Toks prisijungimo vardas jau panaudotas."; break;
                case 3: echo "Blogas prisijungimo vardas ar slaptažodis."; break;
                case 4: echo "Per ilgas prisijungimo vardas."; break;
                case 5: echo "Per ilgas slaptažodis."; break;
                case 6: echo "Per ilgas vardas."; break;
                case 7: echo "Per ilgas hobis."; break;
                case 8: echo "Per ilgas vienas iš pasirenkamų laukelių."; break;
            }
        }
        echo "</div></div></div></div>";
        return;
    }
    public function formMenu($text){
        $text=$this->badWordFilter($text);
        if($text=="")return;
        echo "Apie laukelis turi:<br>";

        echo "Sakiniai: <input name='ifSentences' ";
        if($_POST["ifSentences"]??""=="on")echo "checked";
        echo " id='ifSentences' type='checkbox' onchange='toggleElement(\"ifSentences\",\"sentences\")'>";
        $sentenceCount = $this->countSentences($text);
        if($sentenceCount==0)$sentenceCount++;
        echo "<div style='display:none' id='sentences'> $sentenceCount sakin".$this->wordEnd("sakinys",$sentenceCount).".</div><br>";

        echo "Žodžiai: <input name='ifWords' ";
        if($_POST["ifWords"]??""=="on")echo "checked";
        echo " id='ifWords' type='checkbox' onchange='toggleElement(\"ifWords\",\"words\")'>";
        $wordCount = str_word_count($text);
        echo "<div style='display:none' id='words'> $wordCount žod".$this->wordEnd("žodis",$wordCount).".</div><br>";

        echo "Unikalūs žodžiai: <input name='ifUniqueWords'";
        if($_POST["ifUniqueWords"]??""=="on")echo "checked";
        echo " id='ifUniqueWords' type='checkbox' onchange='toggleElement(\"ifUniqueWords\",\"uniqueWords\")'> ";
        $uniqueWordCount = $this->uniqueWords($text);
        echo "<div style='display:none' id='uniqueWords'> $uniqueWordCount žod".$this->wordEnd("žodis",$uniqueWordCount).".</div><br>";

        echo "Žodžių pasikartojimas: <input name='ifWordCount' ";
        if($_POST["ifWordCount"]??""=="on")echo "checked";
        echo " id='ifWordCount' type='checkbox' onchange='toggleElement(\"ifWordCount\",\"wordCount\")'> ";
        $countWordsArr = $this->wordUsage($text);
        echo "<div style='display:none' id='wordCount'> ";
        foreach ($countWordsArr as $word=>$count){
            echo $word."[".$count."] ";
        }
        echo "</div><br>";

        echo "Blogi žodžiai: <input name='ifBadWordCount' ";
        if($_POST["ifBadWordCount"]??""=="on")echo "checked";
        echo " id='ifBadWordCount' type='checkbox' onchange='toggleElement(\"ifBadWordCount\",\"badWordCount\")'> ";
        $countBadWordsArr = $this->badWordUsage($text);
        echo "<div style='display:none' id='badWordCount'> ";
        foreach ($countBadWordsArr as $word=>$count){
            echo $word."[".$count."] ";
        }
        echo "</div><br>";
        $ifs=["ifSentences","ifWords","ifUniqueWords","ifWordCount","ifBadWordCount"];
        $divs=["sentences","words","uniqueWords","wordCount","badWordCount"];
        $i=0;
        echo "<script>";
        foreach($ifs as $if){
            echo "toggleElement('$if','$divs[$i]');";
            $i++;
        }
        echo "</script>";
    }
    private function lastDigit($num){
        return $num%10;
    }
    private function countSentences($str){
        $count=0;
        $prev="";
        $punctuation=[".","!","?"];
        foreach(str_split($str) as $letter){
            if(self::isInArray($punctuation,$letter)&&!self::isInArray($punctuation,$prev))$count++;
            $prev=$letter;
        }
        return $count;
    }
    private function uniqueWords($str){
        $str=self::removeChars($this->trim_full($str,false),"!.?");
        $usedWords=[];
        $count=0;
        foreach(explode(" ",$str) as $word){
            if(!self::isInArray($usedWords,$word))$count++;
            $usedWords[]=$word;
        }
        return $count;
    }
    public static function removeChars($str,$chars){
        
        foreach(str_split($chars) as $char){
            $str=str_replace($char,"",$str);
        }
        return $str;
    }
    private function wordUsage($str){
        $str = strtolower(self::removeChars($this->trim_full($str,false),".!?"));
        $arr=explode(" ",$str);
        $countArr=[];
        foreach($arr as $word){
            if(!self::isInArrayKeys($countArr,$word))$countArr[$word]=1;
            else $countArr[$word]=$countArr[$word]+1;
        }
        arsort($countArr);
        array_splice($countArr,5);
        return $countArr;
    }
    private function badWordUsage($str){
        $str=strtolower(self::removeChars($this->trim_full($str,false),".!?"));
        $arr=explode(" ",$str);
        $countArr=[];
        foreach($this->badWords as $badWord){
            str_replace($badWord,"",$arr,$count);
            if($count>0)$countArr[$badWord]=$count;
        }
        arsort($countArr);
        return $countArr;
    }
    private static function isInArray($arr,$val){
        foreach($arr as $x){
            if($x==$val)return 1;
        }
        return 0;
    }
    
    private static function isInArrayKeys($arr,$val){
        foreach($arr as $x=>$y){
            if($x==$val)return 1;
        }
        return 0;
    }
    private $badWords=["žalia rūta","fuck","china bad","taiwan is a country","😠"];
    public function badWordFilter($str){
        return str_ireplace($this->badWords,"😠",$this->trim_full($str,false));
    }
    
    private function wordEnd($word,$num){
        $last = $this->lastDigit($num);
        if($word=="žodis"){
            if($last==1&&$num!=11)return "is";
            else if($last>1&&($num<10||$num>20))return "žiai";
            else return "žių";
        }
        if($word=="sakinys"){
            if($last==1&&$num!=11)return "ys";
            else if($last>1&&($num<10||$num>20))return "iai";
            else return "ių";
        }
    }
    public function formStyle(){
        echo "<style>
        :root{
            --font-size:2.5vmin; 
        }
        b{
            color:red;
        }
        #ifHobby{
            width:28px;
            height:28px;
        }
        div.form{
            font-size: clamp(18px,var(--font-size),70px);
            display: block;
            text-align: left;
            border:darkslategrey solid 3px;
            border-radius: 0px;
            padding:0px;
            max-width: clamp(400px,65vw,700px);
            background-color: rgb(245,255,245);
            margin-bottom: 0;
        }
        .form li{
            list-style:none;
            border:1px solid rgba(200,230,200,0.5);
            padding-bottom: 3px;
            padding-top: 3px;
            min-height: 5.7vh;
        }
        input[type=radio]{
            height: 15px;
            width: 15px;
        }
        textarea{
            width:100%;
            padding:0;
        }
        .right{
            float:right;
            margin-bottom: 0;
        }
        input[type=submit]{
            width:100%;
        }
        input[type=text],input[type=password],select{
            width:clamp(200px,30vw,430px);
            border-radius: 2px;
            border:1.5px solid black;
        }
        input[type=color]{
            width:100%;
        }
    </style>";
    }
};
?>