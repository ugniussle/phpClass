<?php
    session_start();
    require "ServerClass.php";
    $serv = new Server();
    $serv->style();
    $serv->menu();
    if($_SESSION["login"]==false){
        header("Location: login.php?prev=".htmlspecialchars($_SERVER["PHP_SELF"]));
        exit();
    }
    else{
        echo "Hello, ".$_SESSION["username"].'<br>';
    }
    $data=$serv->userData($serv->test_input($_SESSION["username"],1));
    $dataLabel=["username","password","name","city","gender","hobby","about","blood"];
    for($i=0;$i<8;$i++){
        echo $dataLabel[$i]." - ".$data[0][$i+1]."<br>";
    }
?>