<?php 
    
    require "FormClass.php";
    $serv = new Server();
    $form = new Form();
    $form->formStyle();
    session_start();
    ?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous"></script>
<?php
    $serv->style();
    $serv->menu();
    
    if($_SESSION["login"] == false || $_SESSION["admin"] == false){
        header("Location: login.php?prev=".htmlspecialchars($_SERVER["PHP_SELF"]));
        exit();
    }
    echo "Hello, ".$_SESSION["username"]."<br>";
    echo "add user:<br>";
?>
<script>
    function toggleElement(ifId,elId){
            var el=document.getElementById(ifId);
            if(el.checked==true)document.getElementById(elId).style.display="inline-block";
            else if(el.checked==false)document.getElementById(elId).style.display="none";
        }
        function toggleCheck(id){
            var el=document.getElementById(id);
            if(el.checked==true)document.getElementById(id).checked=false;
            else if(el.checked==false)document.getElementById(id).checked=true;
        }
</script>
<div class="form container-md">
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?>" style="margin-bottom:0;">
        <li>Prisijungimo vardas: <p class="right">  <input type="text" name="username" maxlength="20"></p></li>
        <li>Slaptažodis: <p class="right">          <input type="password" name="password" maxlength="30"></p></li>
        <li>Vardas ir pavardė: <p class="right">    <input type="text" name="name" maxlength="40"></p></li>
        <li>Miestas: <p class="right">
        <select name="city">
            <option disabled value="select">Pasirinkite miestą:</option>
            <?php
                $db = "test";
                $conn = new mysqli("localhost","root","",$db);
                $cities = mysqli_fetch_all((mysqli_query($conn,"SELECT * FROM `cities`")),MYSQLI_ASSOC);
                foreach($cities as $i => $row){
                    foreach($row as $key => $city){
                        if($key=="city"){
                            echo "<option ";
                            if(($formValues["city"] ?? "")==$city)echo "selected";
                            echo " value=\"$city\">$city</option>";
                        }
                    }
                    
                }
            ?>
        </select></p></li>
        <li>Lytis:
        <p class="right">
        <?php
            $genders=["Male","Female","Other"];
            $gendersLt=["Vyras","Moteris","Kita"];
            $i=0;
            foreach($genders as $gender){
                echo "<input type=\"radio\" name=\"gender\" value=\"$gender\" ";
                echo ">&nbsp";
                echo "<label for=\"$gender\">$gendersLt[$i]&nbsp</label>";
                $i++;
            }
        ?>
        </p></li>
        <li>Ar turite hobį?
        <input class="right" id="ifHobby" type="checkbox" name="ifHobby" maxlength="20" onchange="toggleElement('ifHobby','hobby')">
        <div id="hobby" style="display:none">
            <select name="hobby" multiple>
            <?php
                $hobbies = mysqli_fetch_all((mysqli_query($conn,"SELECT * FROM `hobbies`")),MYSQLI_ASSOC);
                foreach($hobbies as $i => $row){
                    foreach($row as $key => $hobby){
                        if($key=="hobby"){
                            echo "<option ";
                            echo " value=\"$hobby\">$hobby</option>";
                        }
                    }
                    
                }
            ?>
            </select>
        </div><br></li>
        <li>Papasakokite šiek tiek apie save:<textarea name="about"></textarea></li>
        <li>Jūsų <b>kraujo</b> spalva: <input type="color" name="color"></li>
        <input type="submit" name="submit" value="Registruotis!">
    </form>
    <?php 
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $form->registerModal();
        echo "<script>var modal = new bootstrap.Modal(document.getElementById('modal'));modal.show();</script>";
        //foreach(array_keys($_POST) as $key) echo "<script>console.log(\"".$key." ".$_POST[$key]."\");</script><br>";
    }
    ?>
</div>