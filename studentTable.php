<?php 
    session_start();
    require "ServerClass.php";
    $serv = new Server();
    $serv->style();
    $serv->menu();
    if($_SESSION["login"]==false){
        header("Location: login.php?prev=".htmlspecialchars($_SERVER["PHP_SELF"]));
        exit();
    }
    else{
        echo "Hello, ".$_SESSION["username"];
    }
?>
<style>
    :root{
            --cell-size:calc(4vh + 0.5vw);
            --font-size:calc(2.5vmin);
        }
        td{
            border:solid green 1px;
            height:calc(var(--cell-size) * 1.2);
            width:calc(var(--cell-size) * 2);
            color:white;
            font-size:var(--font-size);
            font-weight: bold;
            padding-left: 20px;
            padding-right: 20px;
        }
        .studentsTable td:nth-child(1){
            text-align: center;
        }
        .studentsTable td:nth-child(2){
            text-align: right;
        }
        .studentsTable td:nth-child(3){
            text-align: left;
        }
        body{
            background: lightgray;
            color:black;
            font-size:var(--font-size);
        }
        table{
            border-collapse:collapse;
            background:darkslategrey;
        }
</style>
<?php
class StudentTable{
    private $students;
    public function StudentTable($_students=0){
        if($_students==0)$this->students=$this->createTable();
        else if(is_array($_students))$this->students=$_students;
    }
    public function createTable(){
        $arr = array();
        $firstNames = array("Jonas","Antanas","Simonas","Ugnius","Arnas","Džiugas","Eidenis","Lukas","Oskaras","Aleksas");
        $lastNames = array("Jonaitis","Antanaitis","Šlevinskis","Zabėlius");
        $groupcodes = array("JNIN20","JNIN21","JNIN19");
        $usedNames=array();
        foreach($groupcodes as $code){
            $arr[$code]=array();
            for($i=0;$i<10;$i++){
                do{
                    $rndFName=rand(0,sizeof($firstNames)-1);
                    $rndLName=rand(0,sizeof($lastNames)-1);
                    $FName = $firstNames[$rndFName];
                    $LName = $lastNames[$rndLName];
                    }
                while($this->search($FName,$LName,$usedNames)==1);
                $usedNames[]=$FName.' '.$LName;
                $arr[$code][]=array(
                    'Vardas'=>$FName,'Pavardė'=>$LName
                );
            }
        }
        //print_r($arr);
        return $arr;
    }
    public function sortTable(){
        $students = $this->students;
        ksort($students);
        foreach(array_keys($students) as $key){
            usort($students[$key],function($a,$b){
                return strcmp($a['Pavardė'].' '.$a['Vardas'], $b['Pavardė'].' '.$b['Vardas']);
            });
        }
        $this->students = $students;
    }
    public function tableHTML(){
        $students=$this->students;
        $H="<table class='studentsTable'>";
        $H=$H."<tr>";
        $titles = array("Kodas","Vardas","Pavardė");
        foreach($titles as $title){
            $H=$H."<td>".$title."</td>";
        }
        $H=$H."</tr>";
        foreach(array_keys($students) as $key){
            foreach($students[$key] as $students3){
                $H=$H."<tr>";
                $H=$H."<td>".$key."</td>";
                foreach($students3 as $value){
                        $H=$H."<td>".$value."</td>";
                    }
                    $H=$H."</tr>";
            }
        }
        $H=$H."</table>";
        return $H;
        }
        public function search($value1,$value2,$arr){
            foreach($arr as $x){
                if($value1.' '.$value2==$x)return 1;
            }
            return 0;
        }
};
    $x = new StudentTable();
    $x->sortTable();
    echo $x->tableHTML();
    ?>